package com.max.entities;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.max.utils.GeoTypeConverter;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@DynamoDBTable(tableName = "Hotels")
public class Hotel {
    @DynamoDBHashKey
    @DynamoDBGeneratedUuid(DynamoDBAutoGenerateStrategy.CREATE)
    private String id;
    @DynamoDBAttribute
    private String name;
    @DynamoDBTypeConverted(converter = GeoTypeConverter.class)
    @DynamoDBAttribute
    private Geo geo;


}
