package com.max.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@AllArgsConstructor
public class Geo {
    private final Double latitude;
    private final Double longitude;


}
