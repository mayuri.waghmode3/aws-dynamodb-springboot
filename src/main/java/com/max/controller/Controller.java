package com.max.controller;


import com.max.entities.Hotel;
import com.max.repositories.CustomHotelRepository;
import com.max.repositories.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/hotels")
public class Controller {
    @Autowired
    private CustomHotelRepository customHotelRepository;


    @GetMapping("/table")
    public ResponseEntity createTable()   {
        customHotelRepository.createTable();
        return ResponseEntity.ok("Table Created!");
    }
    @GetMapping
    public List<Hotel> readAll() throws IOException {
        return  customHotelRepository.loadData();
    }

    @DeleteMapping
    public String deleteAllItems() throws IOException {
        customHotelRepository.deleteAllItems();
        return "Table items Deleted!";
    }



}
