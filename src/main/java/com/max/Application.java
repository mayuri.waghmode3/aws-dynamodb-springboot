package com.max;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableDynamoDBRepositories("com.max.repositories")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(com.max.Application.class, args);
    }
}
