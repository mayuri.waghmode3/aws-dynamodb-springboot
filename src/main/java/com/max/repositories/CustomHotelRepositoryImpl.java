package com.max.repositories;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.model.*;
import com.max.entities.Geo;
import com.max.entities.Hotel;
import lombok.extern.slf4j.Slf4j;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Slf4j
@EnableScan
public class CustomHotelRepositoryImpl implements CustomHotelRepository {


    private static final String TABLE_NAME= "Hotels";
    @Autowired
    private HotelRepository hotelRepository;
    private DynamoDBMapper dynamoDBMapper;

    @Autowired
    private AmazonDynamoDB amazonDynamoDB;
    /**
     * https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/SQLtoNoSQL.CreateTable.html
     */
    @Override
    public void createTable()  {
        dynamoDBMapper = new DynamoDBMapper(amazonDynamoDB);

        CreateTableRequest tableRequest = dynamoDBMapper
                .generateCreateTableRequest(Hotel.class);
        tableRequest.setProvisionedThroughput(
                new ProvisionedThroughput(1L, 1L));
        amazonDynamoDB.createTable(tableRequest);

    }
    public void deleteAllItems(){
        dynamoDBMapper = new DynamoDBMapper(amazonDynamoDB);
        dynamoDBMapper.batchDelete(hotelRepository.findAll());
    }

    @Override
    public List<Hotel> loadData() {
        Hotel hotel = new Hotel();
        hotel.setName("Mayuri Palace");
        hotel.setGeo(new Geo(51.7823,-3.4959));
        hotelRepository.save(hotel);

        List<Hotel> result
                = (List<Hotel>) hotelRepository.findAll();
        return result;
    }


}
