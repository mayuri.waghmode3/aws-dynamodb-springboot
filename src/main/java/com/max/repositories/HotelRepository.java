package com.max.repositories;

import com.max.entities.Hotel;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
@EnableScan
public interface HotelRepository extends CrudRepository<Hotel,String> {
    List<Hotel> findAllByName(String name);
    Hotel save(Hotel hotel);
}
