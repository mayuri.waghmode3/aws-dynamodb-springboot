package com.max.repositories;


import com.max.entities.Hotel;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
@EnableScan
public interface CustomHotelRepository {
    void createTable();
    List<Hotel> loadData() throws IOException;
    void deleteAllItems();
}
