## Prerequisite tools
    Java 1.8 or higher
    Intellij or any ide
    Maven

## Dynamodb policy and user creation    

1.  Create user give programatic access 
2.  and copy access key and screte to application.properties file.
3.  IAM Policy for accessing DynamoDB : AmazonDynamoDBFullAccess

## Dependencies required

*  spring-data-dynamodb
*  aws-dynamodb-springboot
*  aws-java-sdk-dynamodb
*  lombok

